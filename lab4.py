import bs4 as bs
import networkx as nx
import urllib.request as urllib2
import re
import matplotlib.pyplot as plt
import threading
import os
import pandas as pd
import threading, queue

q = queue.Queue()
maxThreads=50
nextUrl=queue.Queue()
crawlURL=[]


def get_page_link(url):
    # print(url)
    urllist = []
    try:
      res = urllib2.urlopen(url)
      htmlpage=res.read()
    except:
      return urllist

    try:
      page=bs.BeautifulSoup(htmlpage, "html.parser")
    except:
      return urllist

    pattern = re.compile(r"https://*.*.*/*")
    refs = page.findAll("a", href=pattern)
    for a in refs:
      try:
        link = a['href']
        # if link[:4] == 'http':
        if str(link).find("pythonist.ru") != -1:
          urllist.append(link)
      except:
        pass

    # print(urllist)
    return urllist


def link_finder(urlTuple, graph):
    # Crawls to a given depth using a tuple structure to tag urls with their depth
    global crawlURL, nextUrl, maxDepth
    url = urlTuple[0]
    depth = urlTuple[1]
    if (depth < 2) :
      links = get_page_link(url)
      for link in links:
        # These two lines create the graph
        graph.add_node(link)
        graph.add_edge(url,link)
        # If the link has not been crawled yet, add it in the queue with additional depth
        if link not in crawlURL:
          nextUrl.put((link, depth + 1))
          crawlURL.append(link)
    return

class CrawlThread(threading.Thread):
    def __init__(self,queue,graph):
      threading.Thread.__init__(self)
      self.toCrawl=queue
      self.graph=graph
      while self.toCrawl.empty() is False:
        link_finder(self.toCrawl.get(), self.graph)

def draw_graph(graph, graphName):
	nx.draw(graph, withLabels=False)
	nx.write_dot(graph, os.cwd() + graphName + '.dot')
	plt.savefig(os.cwd() + graphName + '.png')

def calculate_page_rank(url):
  print(str(url))
  URLRoot = url
  parsingFlag = 'beautifulsoup'
  maxDepth=2

  nextUrl.put((URLRoot, 0))
  crawlURL.append(URLRoot)
  ipList=[]
  g = nx.Graph()
  g.add_node(URLRoot)
  threadList = []

  for i in range(maxThreads): #changed
    t=CrawlThread(nextUrl, g)
    t.daemon=True
    t.start()
    threadList.append(t)

  for t in threadList:
    t.join()

  pagerank = nx.pagerank_numpy(g, alpha=0.5, personalization=None,  weight='weight', dangling=None)
  # print(pagerank)
  pg = sorted(pagerank)
  # print(pg)
  k = 1
  for i in pg:
      if k<=10:
        print(str(i) + " " + str(pagerank.get(i)))
        k+=1

  edgeNumber = g.number_of_edges()
  nodeNumber = g.number_of_nodes()
  nodesize=[g.degree(n)*10 for n in g]
  pos=nx.spring_layout(g,iterations=20)

  nx.draw(g,with_labels=False)
  nx.draw_networkx_nodes(g,pos,node_size=nodesize,node_color='r')
  nx.draw_networkx_edges(g,pos)
  plt.figure(figsize=(5,5))
  plt.show()

  return pd.Series([pagerank.get(url), edgeNumber, nodeNumber], index=['pagerank','edges', 'nodes'])

url = 'https://pythonist.ru/'
calculate_page_rank(url)